import tkinter as tk
import tkinter.messagebox
import tkinter.filedialog
from PIL import Image, ImageTk

window = tk.Tk()
window.geometry("600x378")
window.title('SCORE VOLLEY')
window.resizable(width=False, height=False)

background_image = Image.open("stade.png")
background_photo = ImageTk.PhotoImage(background_image)

canvas = tk.Canvas(window, width=600, height=378)
canvas.grid(row=0, column=0, sticky="nsew")
canvas.create_image(0, 0, image=background_photo, anchor="nw")

def changescore(a,aa,b):
    
    global serviceEquipe
    if serviceEquipe == aa:
        service()
    
    nbsetEq1=0
    nbsetEq2=0
    for i in totalscore:
        if i[0]>i[1]:
            nbsetEq1+=1
        else:
            nbsetEq2+=1
    file1 = open(pathTEXTFILE+"nbSetE1.txt", "w")
    file2 = open(pathTEXTFILE+"nbSetE2.txt", "w")
    file1.write(str(nbsetEq1))
    file2.write(str(nbsetEq2))
    file1.close()
    file2.close()
    if nbsetEq1==3 or nbsetEq2==3:
        findumatch[0]=True
    else:
        findumatch[0]=False
    z=""
    listz=""
    for i in totalscore:
        if len(z)>0:
            z+="\n"
        tempo=str(i[0]) + "-" + str(i[1])
        z+=tempo
        listz+="  "+tempo
    labelSet.config(text=z)
    file3 = open(pathTEXTFILE+"Set.txt","w")
    file3.write(listz)


    if b == 0:
        a[0]=0
        a[1]=0
    elif b==-1 or b==1:
        a[aa]+=b
        if a[aa]<0:
            a[aa]=0
    if findumatch[0] ==True:
        a=['','']

    SCOREE1label.config(text=str(a[0]))
    SCOREE2label.config(text=str(a[1]))
    file1 = open(pathTEXTFILE+"E1.txt", "w")
    file2 = open(pathTEXTFILE+"E2.txt", "w")
    file1.write(str(a[0]).rjust(2," "))
    file2.write(str(a[1]).rjust(2," "))
    file1.close()
    file2.close()

def service():
    global serviceEquipe
    if serviceEquipe==0:
        serviceEquipe=1
    else:
        serviceEquipe=0
    file1 = open(pathTEXTFILE+"ServiceE1.txt", "w")
    file2 = open(pathTEXTFILE+"ServiceE2.txt", "w")
    if serviceEquipe==1:
        file1.write('.')
        file2.write('')
        canvas.create_window(125,190, window=serveur)

    else:
        file1.write('')
        file2.write('.')
        canvas.create_window(475,190, window=serveur)

    file1.close()
    file2.close()

def coherenceScore(a, b=25):
    diffdiff = abs(a[0]-a[1])
    if diffdiff<2:
        return False
    if a[0]<b and a[1]<b :
        return False
    if a[0]>b or a[1]>b:
        if diffdiff!=2:
            return False
    return True

def AddSetScore():
    if len(totalscore)<5:
        if len(totalscore)==4:
            bbb=15
        else:
            bbb=25
        if coherenceScore(scoreEQUIPES,bbb):
            totalscore.append([scoreEQUIPES[0],scoreEQUIPES[1]])
            changescore(scoreEQUIPES,0,0)


def RemoveSetScore():
    if len(totalscore)>0:
        scoreEQUIPES[0] = totalscore[len(totalscore)-1][0]
        scoreEQUIPES[1] = totalscore[len(totalscore)-1][1]
        del(totalscore[len(totalscore)-1])
        changescore(scoreEQUIPES,1,2)




pathTEXTFILE="./"
scoreEQUIPES=[0,0]
totalscore = []
findumatch = [False]
serviceEquipe=0


#TEXT_TEAM_1:
toto=input('Nom equipe 1 : ')
label1 = tk.Label(
    canvas,
    text=toto,
    foreground="white",  # Set the text color to white
    background="#318CE7",  # Set the background color to black
    width=len(toto),
    height=1
)
canvas.create_window(125, 66, window=label1) #POSITION_DU_BOUTON

#SCORE_TEAM_1:
SCOREE1label = tk.Label(
    canvas,
    text=str(scoreEQUIPES[0]),
    foreground="white",  # Set the text color to white
    background="black",  # Set the background color to black
    width=3,
    height=1
)
canvas.create_window(190, 190, window=SCOREE1label) #POSITION_DU_BOUTON

#BUTTON "-1" TEAM_1:
buttonE1M = tk.Button(
    canvas,
    text="-1",
    width=5,
    height=1,
    background="#FF504C",
    fg="white",
    command=lambda:changescore(scoreEQUIPES,0,-1)
)
canvas.create_window(125, 250, window=buttonE1M) #POSITION_DU_BOUTON

#BUTTON "+1" TEAM_1:
buttonE1P = tk.Button(
    canvas,
    text="+1",
    width=5,
    height=1,
    background="#25D46C",
    fg="white",
    command=lambda:changescore(scoreEQUIPES,0,1)
)
canvas.create_window(125, 130, window=buttonE1P) #POSITION_DU_BOUTON

#TEXT_TEAM_2:
toto=input('Nom equipe 2 : ')
label2 = tk.Label(
    canvas,
    text=toto,
    foreground="white",  # Set the text color to white
    background="#318CE7",  # Set the background color to black
    width=len(toto),
    height=1
)
canvas.create_window(475, 66, window=label2) #POSITION_DU_BOUTON

#SCORE_TEAM_2:
SCOREE2label = tk.Label(
    canvas,
    text=str(scoreEQUIPES[1]),
    foreground="white",  # Set the text color to white
    background="black",  # Set the background color to black
    width=3,
    height=1
)
canvas.create_window(410, 190, window=SCOREE2label) #POSITION_DU_BOUTON

#BUTTON "-1" TEAM_2:
buttonE2M = tk.Button(
    canvas,
    text="-1",
    width=5,
    height=1,
    background="#FF504C",
    fg="white",
    command=lambda:changescore(scoreEQUIPES,1,-1)
)
canvas.create_window(475, 250, window=buttonE2M) #POSITION_DU_BOUTON

#BUTTON "+1" TEAM_2:
buttonE2P = tk.Button(
    canvas,
    text="+1",
    width=5,
    height=1,
    background="#25D46C",
    fg="white",
    command=lambda:changescore(scoreEQUIPES,1,1)
)
canvas.create_window(475, 130, window=buttonE2P) #POSITION_DU_BOUTON

#BUTTON "RESET":
buttonReset = tk.Button(
    canvas,
    text="RST",
    width=5,
    height=1,
    background="#C3782F",
    fg="black",
    command=lambda:changescore(scoreEQUIPES,0,0)
)
canvas.create_window(25, 15, window=buttonReset) #POSITION_DU_BOUTON

#BUTTON "ADD_SET":
buttonAddSet = tk.Button(
    canvas,
    text="AddSet",
    width=8,
    height=1,
    background="#FFDB58",
    fg="black",
    command=AddSetScore
)
canvas.create_window(395, 345, window=buttonAddSet) #POSITION_DU_BOUTON

#BUTTON "REMOVE_SET":
buttonRemoveSet = tk.Button(
    canvas,
    text="RemoveSet",
    width=8,
    height=1,
    background="#FFDB58",
    fg="black",
    command=RemoveSetScore
)
canvas.create_window(205, 345, window=buttonRemoveSet) #POSITION_DU_BOUTON

buttonSERVICE = tk.Button(
    canvas,
    text="SERVICE",
    width=6,
    height=1,
    background="#C3782F",
    fg="black",
    command=lambda:service()
)

canvas.create_window(80, 15, window=buttonSERVICE) #POSITION_DU_BOUTON

#SERVICE_TEAM:
serveur_image = Image.open("ball2.png")
serveur_photo = ImageTk.PhotoImage(serveur_image)

serveur = tk.Label(
    canvas,
    width=30,
    height=30,
    image=serveur_photo,
    background="#F5C87C"
)

#SCORE_TABLE
labelSet = tk.Label(
    canvas,
    text="",
    foreground="white",
    background="#C3782F",
    width=10,
    height=5,
    borderwidth=3,
    relief="solid"
)
canvas.create_window(300, 330, window=labelSet) #POSITION_DU_BOUTON

window.mainloop()